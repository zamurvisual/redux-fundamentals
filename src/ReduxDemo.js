import React, { Component } from 'react';
import { createStore } from 'redux';

export default class ReduxDemo extends Component {
    render() {

        // step2 Reducer: state and action
        const reducer = (state, action) => {
            if (action.type === "ATTACK") {
                return action.payload
            }

            if (action.type === "GREEN_ATTACK") {
                return action.payload
            }

            return state;
        }

        // step1 Store: reducer and state
        const store = createStore(reducer, "Peace")

        // step3 Subscribe
        store.subscribe(() => {
            console.log("Store is now", store.getState());
        })

        //step4 Dispatch action
        store.dispatch({ type: "ATTACK", payload: "Iron Man" })
        store.dispatch({ type: "GREEN_ATTACK", payload: "Hulk" })

        return (
            <div>
                test
            </div>
        )
    }
}
