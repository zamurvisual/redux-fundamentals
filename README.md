# App
```
npm i --save redux
npm i --save react-redux
```

On App.js

```javascript
    import ReduxDemo from './ReduxDemo';
    <ReduxDemo />
```

# Usigin Redux on React

+ Starting with a plain React app with component state
+ Installing the redux and react-redux libraries
+ Creating a Redux store
+ Writing a reducer
+ Dispatching actions
+ Using `Provider` to pass the Redux state all across the app
+ Using `connect` to get the Redux state into a React component
+ How to write `mapDispatchToProps` (and what it does)
+ How to dispatch actions from a React component, update the state, and render the change



## Steps
On ReduxDemo.js

1. create a 'store'

    necessita de dois parâmetros: reducer e state (initial state)

```javascript
    import { createStore } from 'redux';

    // dentro do render()
    const store = createStore(reducer, "Peace")
```

2. reducer

    necessita de dois parâmetros: state e action

    state: o que está acontecendo no momento

    action: action para se fazer de acordo com o state

```javascript
    // step2 Reducer: state and action
    const reducer = (state, action) => {
        if (action.type === "ATTACK") {
            return action.payload
        }

        if (action.type === "GREEN_ATTACK") {
            return action.payload
        }

        return state;
    }
```

3. subscribe: getting the current state/ getting connect to the database

```javascript
    // step3 Subscribe
    store.subscribe(() => {
        console.log("Store is now", store.getState());
    })
```


4. dispatch: action you need to take

```javascript
    //step4 Dispatch action
    store.dispatch({ type: "ATTACK", payload: "Iron Man" })
    store.dispatch({ type: "GREEN_ATTACK", payload: "Hulk" })
```